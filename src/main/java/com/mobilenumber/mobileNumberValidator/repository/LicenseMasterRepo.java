package com.mobilenumber.mobileNumberValidator.repository;

import com.mobilenumber.mobileNumberValidator.model.LicenseManager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LicenseMasterRepo extends JpaRepository<LicenseManager, Long> {

    LicenseManager findByliCode(String liCode);
}
