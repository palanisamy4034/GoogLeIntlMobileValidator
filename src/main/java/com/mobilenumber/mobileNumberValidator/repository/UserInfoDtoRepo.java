package com.mobilenumber.mobileNumberValidator.repository;

import com.mobilenumber.mobileNumberValidator.dto.UserInfoDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserInfoDtoRepo extends JpaRepository<UserInfoDto, Long> {
    @Query(value = "SELECT u.id as id,u.username as username,u.email as email," +
            "u.source as source,u.mobile as mobile ,l.li_type as li_type,l.li_code as li_code," +
            "l.no_req as no_req,l.aviled_req as aviled_req ,l.visitor_domain as visitor_domain," +
            "l.expired_date as expired_date FROM license_master l," +
            "user_info u where u.id=l.user_id",nativeQuery = true)
    List<UserInfoDto> findUsers();

}
