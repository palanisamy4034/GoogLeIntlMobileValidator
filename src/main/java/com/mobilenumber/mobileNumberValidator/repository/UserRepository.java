package com.mobilenumber.mobileNumberValidator.repository;

import com.mobilenumber.mobileNumberValidator.dto.UserInfoDto;
import com.mobilenumber.mobileNumberValidator.model.User;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByemail(String email);

}