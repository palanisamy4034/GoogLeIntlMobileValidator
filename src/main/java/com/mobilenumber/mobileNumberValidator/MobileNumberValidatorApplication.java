package com.mobilenumber.mobileNumberValidator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobileNumberValidatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobileNumberValidatorApplication.class, args);
	}
}
