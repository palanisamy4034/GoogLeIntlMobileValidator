package com.mobilenumber.mobileNumberValidator.dto;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@ToString
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
public class UserInfoDto {
    @Id
    @Column(name= "id")
    private Long id;
    private String username;
    private Long mobile;
    private String email;
    private Integer source;
    private Integer liType;
    private String liCode;
    private Long aviledReq;
    private Long noReq;
    private String visitorDomain;
    private Date expiredDate;
}