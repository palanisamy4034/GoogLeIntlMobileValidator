package com.mobilenumber.mobileNumberValidator.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class NumValidDTO {
    private String mobile;
    private String country;
    private String licCode;
}
