package com.mobilenumber.mobileNumberValidator.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class MailObject {
    @NotNull
    @Size(min = 1, message = "Please, set an email address to send the message to it")
    private String to;
    private String subject;
    private String text;

}

