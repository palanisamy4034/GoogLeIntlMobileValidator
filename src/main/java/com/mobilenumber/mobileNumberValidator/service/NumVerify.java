package com.mobilenumber.mobileNumberValidator.service;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.google.i18n.phonenumbers.ShortNumberInfo;
import com.mobilenumber.mobileNumberValidator.dto.NumValidDTO;
import com.mobilenumber.mobileNumberValidator.model.LicenseManager;
import com.mobilenumber.mobileNumberValidator.model.MobileNumber;
import com.mobilenumber.mobileNumberValidator.repository.LicenseMasterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NumVerify {

    @Autowired
    LicenseMasterRepo licenseMasterRepo;

    public Object addUser(NumValidDTO numValidDTO) {

        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        ShortNumberInfo shortInfo = ShortNumberInfo.getInstance();
        LicenseManager licenseManager=licenseMasterRepo.findByliCode(numValidDTO.getLicCode());
        if(licenseManager!=null) {
            MobileNumber mobileResStatus = new MobileNumber();
            Phonenumber.PhoneNumber number = null;
            if (numValidDTO.getMobile() != null && numValidDTO.getCountry() != null) {
                try {

                    mobileResStatus.setMobileNo(numValidDTO.getMobile());
                    mobileResStatus.setCountryCode(numValidDTO.getMobile());
                    mobileResStatus.setSampleNumber(String.valueOf(phoneUtil.getExampleNumber(numValidDTO.getCountry())));

                    number = phoneUtil.parseAndKeepRawInput(numValidDTO.getMobile(), numValidDTO.getCountry());
                    boolean isNumberValid = phoneUtil.isValidNumber(number);
                    String prettyFormat = isNumberValid
                            ? phoneUtil.formatInOriginalFormat(number, numValidDTO.getCountry())
                            : "invalid";
                    String internationalFormat = isNumberValid
                            ? phoneUtil.format(number, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL)
                            : "invalid";

                    mobileResStatus.setIsNumberValid(isNumberValid);
                    mobileResStatus.setInternationalFormat(internationalFormat);

                } catch (NumberParseException e) {
                    e.printStackTrace();
                }

                return mobileResStatus;
            }
        }
        return "{'status':'false','data':'Input is Notnull / Empty'}";
    }

}
