package com.mobilenumber.mobileNumberValidator.service;

import com.mobilenumber.mobileNumberValidator.dto.UserInfoDto;
import com.mobilenumber.mobileNumberValidator.model.LicenseManager;
import com.mobilenumber.mobileNumberValidator.model.ResponseClass;
import com.mobilenumber.mobileNumberValidator.model.User;
import com.mobilenumber.mobileNumberValidator.repository.LicenseMasterRepo;
import com.mobilenumber.mobileNumberValidator.repository.UserInfoDtoRepo;
import com.mobilenumber.mobileNumberValidator.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;


@Component
public class UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    UserRepository userRepo;
    @Autowired
    UserInfoDtoRepo userRepo1;
    @Autowired
    LicenseMasterRepo licenseMasterRepo;

    @Autowired
    EmailValidator emailValidator;

    @Autowired
    private EmailService emailService;

    public ResponseClass addUser(User userInfo) {
        User userInfo1 = userRepo.findByemail(userInfo.getEmail());
        ResponseClass responseClass = new ResponseClass();
        Date currentDate = new Date();
        long unix = currentDate.getTime() / 1000;
        if (userInfo1 == null) {
            //userInfo1.setUsername(userInfo1.getUsername() == null ? emailValidator.getUsernameByEmailId(userInfo.getEmail()) : userInfo1.getUsername());
            userInfo1 = userRepo.save(userInfo);
            LicenseManager licenseManager = new LicenseManager();
            licenseManager.setLiType(0);
            licenseManager.setLiCode(Base64.getEncoder().encodeToString((emailValidator.getUsernameByEmailId(userInfo.getEmail()) + unix).getBytes()));
            licenseManager.setUserId(userInfo1.getId());
            licenseManager.setAviledReq((long) 0);
            licenseManager.setNoReq((long) 200);
            //licenseManager.setVisitorDomain(userInfo1.);
            licenseMasterRepo.save(licenseManager);
            responseClass.setStatus(true);
            responseClass.setMsg("New customer");
            responseClass.setUsername(userInfo1.getUsername());
            responseClass.setId(userInfo1.getId());
            responseClass.setLiCode(licenseManager.getLiCode());
            emailService.sendMail(userInfo.getEmail(), "TEST", "welcome");
        } else {
            responseClass.setStatus(true);
            responseClass.setMsg("Existing customer");
            responseClass.setUsername(userInfo1.getUsername());

            responseClass.setId(userInfo1.getId());
        }
        return responseClass;

    }


    public Object getAllUser() {
        logger.info(userRepo1.findUsers().toString());
        List<UserInfoDto> userInfos = userRepo1.findUsers();
        logger.info(String.valueOf(userInfos));
        List<UserInfoDto> userInfoDtoList = new ArrayList<>();
        for (UserInfoDto userInfo : userInfos) {
            UserInfoDto userInfoDto = new UserInfoDto();
            userInfoDto.setEmail(userInfo.getEmail());
            userInfoDto.setUsername(userInfo.getUsername());
            userInfoDto.setId(userInfo.getId());
            userInfoDto.setSource(userInfo.getSource());
            userInfoDto.setMobile(userInfo.getMobile());
            userInfoDto.setLiCode(userInfo.getLiCode());
            userInfoDto.setLiType(userInfo.getLiType());
            userInfoDto.setExpiredDate(userInfo.getExpiredDate());
            userInfoDto.setAviledReq(userInfo.getAviledReq());
            userInfoDto.setNoReq(userInfo.getNoReq());
            userInfoDto.setVisitorDomain(userInfo.getVisitorDomain());
            userInfoDtoList.add(userInfoDto);
        }
        return userInfoDtoList;
    }

}
