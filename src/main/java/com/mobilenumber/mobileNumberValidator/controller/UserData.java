package com.mobilenumber.mobileNumberValidator.controller;

import com.mobilenumber.mobileNumberValidator.model.User;
import com.mobilenumber.mobileNumberValidator.service.EmailValidator;
import com.mobilenumber.mobileNumberValidator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserData {

    @Autowired
    UserService userService;
    @Autowired
    EmailValidator emailValidator;

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public Object addUser(@RequestBody User userInfo) {
        if(!emailValidator.validateEmail(userInfo.getEmail()))
            return "{'status':false,'msg':'Email is required/not valid'}";
        return userService.addUser(userInfo);
    }
    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    public Object getUser() {
        return userService.getAllUser();
    }

}