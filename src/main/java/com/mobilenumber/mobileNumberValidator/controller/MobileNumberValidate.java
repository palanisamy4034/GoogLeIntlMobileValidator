package com.mobilenumber.mobileNumberValidator.controller;

import com.mobilenumber.mobileNumberValidator.dto.NumValidDTO;

import com.mobilenumber.mobileNumberValidator.service.NumVerify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class MobileNumberValidate {

    @Autowired
    NumVerify numVerifyService;

    @RequestMapping(value = "/mobile", method = RequestMethod.POST)
    public Object addUser(@RequestBody NumValidDTO numValidDTO,HttpServletRequest http) {
        System.out.println(http.getLocalAddr());
        if(numValidDTO.getCountry()==null || numValidDTO.getCountry().length()>2 || numValidDTO.getCountry().length()<2)
            return "{'status':'False','msg':'Country not null or 2 char '}";
        if(numValidDTO.getMobile()==null)
            return "{'status':'False','msg':'Mobile number not null'}";
        if(numValidDTO.getLicCode()==null)
            return "{'status':'False','msg':'Lic Code not null or 2 char '}";

        return numVerifyService.addUser(numValidDTO);
    }

}