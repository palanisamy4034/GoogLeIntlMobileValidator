package com.mobilenumber.mobileNumberValidator.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MobileNumber {
    @NonNull
    private String mobileNo;
    private String CountryCode;
    private Boolean isNumberValid;
    private String internationalFormat;
    private String sampleNumber;
    private String TypeFormatter;
}
