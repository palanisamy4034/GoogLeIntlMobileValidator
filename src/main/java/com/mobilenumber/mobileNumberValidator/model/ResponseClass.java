package com.mobilenumber.mobileNumberValidator.model;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class ResponseClass extends User {
        private String msg;
        private Boolean status;
        private String liCode;
        private String UserType;
    }


