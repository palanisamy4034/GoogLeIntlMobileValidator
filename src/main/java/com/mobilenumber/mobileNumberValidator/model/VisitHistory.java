package com.mobilenumber.mobileNumberValidator.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "visit_history")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class VisitHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    private String userId;
    private int visit_count;
    private String Visitor_ip;
    private String visitor_domain;
    @LastModifiedDate
    private Date updatedTime;

}
