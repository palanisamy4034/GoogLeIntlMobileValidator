CREATE TABLE user_info (
  `id` INT NOT NULL auto_increment PRIMARY KEY,
  username VARCHAR(50) NOT NULL,
  mobile INTEGER,
  source varchar(50),
  email VARCHAR(50) NOT NULL,
  password CHAR(25),
  created_time datetime,
  updated_time datetime
);

CREATE TABLE license_master (
  `id` INT NOT NULL auto_increment PRIMARY KEY,
  user_id INT,
  aviled_req INT,
  no_req INT,
  li_type INTEGER,
  li_code VARCHAR(50) NOT NULL,
  visitor_domain VARCHAR(250),
  created_time datetime,
  updated_time datetime,
  expired_date datetime,
  FOREIGN KEY (user_id) REFERENCES user_info(id)
);

CREATE TABLE visit_history (
  `id` INT NOT NULL auto_increment PRIMARY KEY,
  visit_count INT,
  user_id INTEGER,
  visitor_ip VARCHAR(20) NOT NULL,
  visitor_domain VARCHAR(50) NOT NULL,
  updated_time datetime,
  FOREIGN KEY (user_id) REFERENCES user_info(id)
);
